# Log analytics with Kibana and Grafana based on Elasticsearch

## TODO: rename the repo to log-analytics

ELK-Stack setup based on https://elk-docker.readthedocs.io/ and https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html with grafana added by me.

Paid features of the ELK stack are disabled.

## Download

1. clone the repository to a folder (\~/docker/kibana-grafana in the example) with `git clone https://gitlab.com/twinter/docker-compose-kibana-grafana.git ~/docker/log-analytics`

2. create your own elasticsearch.env based on the provided template file, set a strong password for elasticsearch if you plan on exposing it to the outside.



## Setup

For details see the official guide: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html

1. change the vm.max_map_count kernel parameter to 262144 or more with `sudo sysctl -w vm.max_map_count=262144`

2. add `vm.max_map_count = 262144` to /etc/sysctl.conf to make the above change permanent

3. adapt the memory settings to your system, the setting is in the docker-compose file under services.elasticsearch.environment and is called `- ES_JAVA_OPTS=-Xms6g -Xmx6g`, for details see https://www.elastic.co/guide/en/elasticsearch/reference/current/heap-size.html

4. create the folders for elasticsearch and kibana with `sudo mkdir -p /srv/log-analytics/kibana/{data,logs} /srv/log-analytics/elasticsearch/{data/nodes,logs}`

5. ensure the mapped folders on the host are writable by elasticsearch with `sudo chmod --recursive 777 /srv/log-analytics`

6. create yourself a new superuser in kibana (log in with hte "elastic" account and password set in elasticsearch.yml -> cogwheel in bottom of sidebar -> security -> users)

7. do not forget to configure something to feed data into elasticsearch. One way of doing this can be found here: https://gitlab.com/twinter/docker-compose-log-collector


## Startup

### With the [docker-compose systemd service](https://gitlab.com/twinter/docker-compose-systemd-service) (recommended as a permanent setup)

1. setup the docker-compose systemd service as described here: https://gitlab.com/twinter/docker-compose-systemd-service

2. create a symlink from the projects folder to /etc/docker/compose with `sudo ln -s /home/your_username/docker/log-analytics /etc/docker/compose/log-analytics`

3. start and enable the service with `sudo systemctl enable --now docker-compose@log-analytics`

### With only docker-compose

Run `sudo docker-compose up` in the projects folder on your system.
